#!/usr/bin/python

import i3ipc
import threading
import time
from time import sleep
import os
import ast
import getopt
import sys
import types
import psutil
import gi
import threading
import sched
gi.require_version('Playerctl', '1.0')
from gi.repository import Playerctl, GLib


# Create the Connection object that can be used to send commands and subscribe
# to events.
i3 = i3ipc.Connection()

def battery_info():
    bat_level = int(os.popen('cat /sys/class/power_supply/BAT0/capacity').read())
    bar_status = os.popen('cat /sys/class/power_supply/BAT0/status').read()
    if "Charging" in bar_status:
        return(" " + str(bat_level) + "%")
    if bat_level > 80:
        return(" " + str(bat_level) + "%")
    if bat_level > 60:
        return(" " + str(bat_level) + "%")
    if bat_level > 40:
        return(" " + str(bat_level) + "%")
    if bat_level > 20:
        return(" " + str(bat_level) + "%")
    return(" " + str(bat_level) + "%")


def ps_info():
    return (" " + str(psutil.cpu_percent()) + "%  " + str(psutil.virtual_memory().percent) + "%")

# Print the name of the focused window
def ether_info():
    ether1_output = str(os.popen('ifconfig enp4s0').read())
    ether2_output = str(os.popen('ifconfig enp0s20u1u1').read())
    if ether1_output.find("inet") != -1 or ether2_output.find("inet") != -1:
        return(" Connected")
    else:
        return(" Disconnected")

def wifi_info():
    wifi_output = os.popen('iwconfig wlp5s0').read()
    wifi_data = wifi_output.split()
    ret_str = ""
    for x in wifi_data:
        if "ESSID" in x:
            if x.find("off/any") != -1:
                continue
            ret_str += x.split(":")[1].replace("\"", "")
        if "level" in x:
            pass
            ret_str += " " + x.split("=")[1] + "dBm "
    if ret_str == "":
        ret_str = " No wifi "
    else:
        ret_str = " " + ret_str
    return ret_str

def now_playing():

    ret_str = " "  
    try:
        player = Playerctl.Player()
        playing_str = player.get_artist() + " - " + player.get_title() + " "
        current_time = int((time.time()*5))
        if len(playing_str) < 30:
            ret_str += playing_str
        else:
            for x in range(0, 30):
                ret_str += playing_str[(current_time+x)%len(playing_str)]
    except:
        return (" Yo dawg! Start some music! ")
    if player.props.status == "Playing":
        ret_str += " "
    else:
        ret_str += " "
    return ret_str

workspaces = []
def set_workspaces():
    global workspaces
    ret_str = ""
    cnt = 0
    pre_bg = ""
    pre_fg = ""
    pre_foc = False
    for x in workspaces:
        foc_mod_pre = ""
        foc_mod_post = ""
        cnt += 1
        if cnt == 2:
            #pre_bg = "%{B#333333}"
            #pre_fg = "%{F#404040}"
            pre_bg = "%{B#002733}"
            pre_fg = "%{F#003d4d}"
            cnt = 0
        else:
            #pre_bg = "%{B#404040}"
            #pre_fg = "%{F#333333}"
            pre_bg = "%{B#003d4d}"
            pre_fg = "%{F#002733}"

        if pre_foc == True:
            pre_foc = False
            pre_fg = "%{F#b58900}"

        if x["focused"] == True:
            #ret_str += "%{B#b58900}%{F#002b36}%{F-} " + x["name"] + "%{B-}%{F#002b36}%{F-}"
            foc_mod_pre = "%{F#002b36}"
            foc_mod_post = "%{F-}"
            pre_bg = "%{B#b58900}"
            pre_foc = True

        ret_str += pre_bg + pre_fg + " " + "%{F-}" + foc_mod_pre + x["name"] + foc_mod_post + "%{B-}"
    if pre_foc: ret_str += "%{F#b58900}%{F-}"
    elif cnt == 1: ret_str += "%{F#003d4d}%{F-}"
    else: ret_str += "%{F#002733}%{F-}"
    return ret_str

def date_and_time():
    return (time.strftime(" %d-%m-%Y %{F#003d4d}%{F-}%{B-}%{B#003d4d}  %H:%M:%S"))

str_battery = ""
str_workspaces = ""
str_now_playing = ""
str_wifi_info = ""
str_ethernet = ""
str_ps_info = ""
str_date = ""

def print_out():
    global str_battery
    global str_workspaces
    global str_now_playing
    global str_wifi_info
    global str_ethernet
    global str_ps_info
    global str_date
    print(str_workspaces + "%{c}" + str_now_playing + "%{r}" + "%{F#002733}%{F-}%{B#002733} " + str_wifi_info + "%{F#003d4d}%{F-}%{B-}%{B#003d4d} " + str_ethernet + " %{F#002733}%{F-}%{B-}%{B#002733} " + str_ps_info + " %{F#003d4d}%{F-}%{B-}%{B#003d4d} " + str_battery + " %{F#002733}%{F-}%{B-}%{B#002733} " + str_date + " %{F#b58900}%{F-}%{F#002b36}%{B-}%{B#b58900} %{A:pgrep \"stalonetray\" | xargs kill || stalonetray&:}TeknighT %{A}%{B-}%{F-}")
    sys.stdout.flush()

def update_output():
    global str_battery
    global str_workspaces
    global str_now_playing
    global str_wifi_info
    global str_ethernet
    global str_ps_info
    global str_date
    str_battery = str(battery_info())
    str_workspaces = str(set_workspaces())
    str_now_playing = str(now_playing())
    str_wifi_info = str(wifi_info())
    str_ethernet = str(ether_info())
    str_ps_info = str(ps_info())
    str_date = str(date_and_time())

cnter = 0
while True:
    global str_now_playing
    new_workspaces = i3.get_workspaces()
    if new_workspaces != workspaces:
        workspaces = new_workspaces
        update_output()
        print_out()
    if cnter%2 == 1:
        str_now_playing = str(now_playing())
        print_out()
    if cnter == 50:
        update_output()
        print_out()
        cnter = 0
    sleep(0.1)
    cnter += 1

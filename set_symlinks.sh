#!/bin/bash

ln -s ~/dotfiles/compton/compton.conf ~/.config/compton.conf
ln -s ~/dotfiles/conky ~/.conky
ln -s ~/dotfiles/i3/config ~/.config/i3/config
ln -s ~/dotfiles/i3/i3blocks.conf ~/.config/i3/i3blocks.conf
ln -s ~/dotfiles/xresources/solarized_dark ~/.Xresources
ln -s ~/dotfiles/zsh/.zshrc ~/.zshrc
ln -s ~/dotfiles/zsh/powerline.zsh-theme ~/.oh-my-zsh/themes/powerline.zsh-theme
ln -s ~/dotfiles/vim/vimrc ~/.config/nvim/init.vim
ln -s ~/dotfiles/vim/vimrc ~/.vimrc

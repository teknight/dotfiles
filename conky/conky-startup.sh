killall conky
if [[ $( xrandr | grep "HDMI-1 connected") ]]; then
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/clock" &
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/music_visual" &
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/now_playing" &
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/sidebar_info" &
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/vpn_conky" &
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/weather" &
else
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/clock" -x 930 &
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/music_visual" -x 1458 &
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/now_playing" -x 1040 &
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/sidebar_info" -x 0 &
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/vpn_conky" -x 1310 &
    cd "/home/teknight/.conky"
    conky -c "/home/teknight/.conky/weather" -x 363 &
fi

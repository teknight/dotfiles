#!/usr/bin/perl

use strict;
use warnings;
use POSIX;

my $device = `cat /home/teknight/.conky/conky_sound_level/device`;
chomp($device);
my $sound_level = `parec --device=$device | od -N2 -td2 | head -n1 | cut -d' ' -f2- | tr -d ' ' | perl -pe 's/^-//'`;

if ($sound_level == 0)
{
    system("echo  \"" . (($device == 4) ? 0 : ($device + 1)) . "\"> /home/teknight/.conky/conky_sound_level/device");
}

$sound_level = $sound_level / 2000 ;
$sound_level = $sound_level * 1.2;
$sound_level = 10 if ($sound_level > 10);

system("rm /home/teknight/.conky/conky_sound_level/vol_tim14.png");
my $i = 13;
while ($i >= 0)
{
    system("mv /home/teknight/.conky/conky_sound_level/vol_tim" . $i . ".png /home/teknight/.conky/conky_sound_level/vol_tim" . ($i+1) . ".png");
    $i--;
}
system("cp /home/teknight/.conky/conky_sound_level/vols/vol" . ceil($sound_level) . ".png /home/teknight/.conky/conky_sound_level/vol_tim0.png");

#!/usr/bin/python

import os
import ast

vpn_on = False

if int(os.popen("ps aux | grep \"openvpn\" | wc -l").read()) >= 4:
    vpn_on = True

ip_info = ast.literal_eval(os.popen("curl -s http://ip-api.com/json").read())
os.system("wget http://flags.fmcdn.net/data/flags/normal/" + ip_info["countryCode"].lower() + ".png -q -O flag.png")
print("Land: " + ip_info["country"])
print("By: " + ip_info["city"].split()[0])
print("Ip: " + ip_info["query"])
print("Vpn on: " + str(vpn_on))
